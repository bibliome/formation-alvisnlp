# Exercice 2 : Projecteurs

Nous apprendrons dans cet exercice qu'il est possible de manipuler des fichiers dans différents formats:

- chargement de documents aux format PubMed XML ou PDF,
- projection des entrées d'une ontologie aux formats RDF ou OBO.

Les entités recherchées sont de nature différente.
Ce ne sont plus des *désignateur rigides*, c'est à dire des noms aux formes stabilisées par une pratique respectueuse d'une nomenclature.
Il s'agit plutôt de groupes nominaux dont l'utilisation peut s'avérer très variables.

Nous observerons que les étiquettes des concepts d'une ontologie sont souvent chevauchantes, et nous apprendrons à ne conserver que les plus longues.


## Répertoire de travail

```
exercices/2-projecteurs
```

## Exercices

### Observer [`projecteurs.plan`](projecteurs.plan)

Ce workflow ressemble encore aux précédents:
1. lecture de documents au format PubMed XML,
2. projection d'un dictionnaire au format tabulé,
3. export des annotations.

### Projeter les concepts de Gene Ontology

Gene Ontology (GO) se trouve dans le répertoire `../share/` en deux formats: OWL et OBO.
Choisir un des deux formats et ajouter dans le plan un module pour projeter les concepts de GO. <!-- préciser la commande à moins que ce soit voulu ainsi : dire de mettre les matches dans un nouveau layer. Faudra aider les participants ici, partie un peu difficile pour un novice -->

Les différents modules permettant de projeter des entrées sont classés par format et documentés à l'adresse suivante: [https://bibliome.github.io/alvisnlp/reference/Module-reference#projectors](https://bibliome.github.io/alvisnlp/reference/Module-reference#projectors).

Observer les annotations obtenues avec le navigateur.

### Retirer les annotations chevauchantes

Ajouter un module de la classe [`RemoveOverlaps`](https://bibliome.github.io/alvisnlp/reference/module/RemoveOverlaps) afin de retirer les annotations chevauchantes.

Comparer les résultats avec ceux obtenus précédemment.

### Traiter les articles entiers fournis au format PDF

Grâce au module [*TikaReader*](https://bibliome.github.io/alvisnlp/reference/module/TikaReader).
