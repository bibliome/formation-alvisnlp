# Exercice 3b : Outils de Reconnaissance d'Entités Nommées

Cet exercice vise à se familiariser avec des outils de REN développés par des tiers et intégrés comme modules dans AlvisNLP.

Nous allons nous intéresser en particulier à:
- `StanfordNLP` pour les entités génériques (personnes, lieux, dates),
- `Chemspot` pour les noms de composés chimiques.

Dans cet exercice, ces outils seront utilisés comme des "boites noires".

## Répertoire de travail

```
exercices/3b-outils-ren
```

## Exercices

### Compléter [`outils-ren.plan`](exercices/3b-outils-ren/outils-ren.plan)

Ce plan ne contient que les étapes de lecture des documents et d'export.
L'exercice consiste à insérer des étapes de façon à détecter des entités nommées génériques et de composés chimiques.

### Détection d'entités génériques

Pour les entités nommées génériques nous allons nous servir de l'outil de REN de `StanfordNER` développé par [l'Université de Stanford](https://nlp.stanford.edu/software/CRF-NER.shtml).
Cet outil est disponible dans AlvisNLP via le module [`StanfordNER`](https://bibliome.github.io/alvisnlp/reference/module/StanfordNER).

Examinez la documentation de ce module et repérerez les paramètres obligatoires.
Parmi ceux-ci, figure `classifierFile` qui spécifie le fichier contenant le modèle que `StanfordNER` doit utiliser.
Vous trouverez différents modèles dans le répertoire `../share/classifiers/`.

`StanfordNER` utilise des techniques d'apprentissage.
Nous verrons à l'exercice 7 comment exploiter l'apprentissage supervisé en détail.
Pour ce module les modèles sont pré-entrainés, et chacun de ces modèles a été entrainé sur des corpus d'apprentissage différent.

Grâce au paramètre `classifierFile`, utilisez différents modèles et comparez les résultats grâce au *browser*.
Observez les features des annotations produites par `StanfordNER`.

Estimez-vous les annotations de `StanfordNER` de bonne qualité?
Comment expliquez-vous ses performances?


### Détection des noms de composés chimiques

Pour les composés chimiques nous utiliserons [Chemspot](https://www.informatik.hu-berlin.de/de/forschung/gebiete/wbi/resources/chemspot/chemspot) développé par l'Université Humboldt (Berlin).
Cet outil est disponible dans AlvisNLP via le module [`Chemspot`](https://www.informatik.hu-berlin.de/de/forschung/gebiete/wbi/resources/chemspot/chemspot).

Examinez la documentation de ce module.

Il n'y a aucun paramètre obligatoire cependant nous spécifierons `noDict` afin de réduire la mémoire requise par cet outil.

Observez les résultats grâce au *browser*.
Observez les features des annotations produites pas `Chemspot`.

Estimez-vous les annotations de `StanfordNER` de bonne qualité?
