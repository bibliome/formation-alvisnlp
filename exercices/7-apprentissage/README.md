# Exercice 7 : Apprentissage

Cet exercice permettra de se familiariser avec l'utilisation de l'apprentissage dans le cadre de la reconnaissance d'entités nommées.
Nous utiliserons [Wapiti](https://wapiti.limsi.fr/), un outil d'apprentissage basé sur les champ aléatoire conditionnel ([conditional random field, CRF](https://en.wikipedia.org/wiki/Conditional_random_field)).

Les CRF sont un ensemble d'algorithmes discrimminants par des modèles probabilistes graphiques souvent utilisés sur des données séquentielles (texte, séquences biologiques).
En reconnaissance d'entités nommées, leur principale force est de pouvoir prendre en compte différentes propriétés des tokens et de leur contexte.
Les systèmes fonctionnels de REN basés sur des CRF ont des performances proches de l'état de l'art.
De plus l'apprentissage et la prédiction sont extrêmement rapides.

La représentation des entités nommées de prédilection pour les CRF est la notation BIO.
Les CRF apprennent à associer à chaque token de la phrase un label BIO.
L'entraînement et la prédiction avec Wapiti dans AlvisNLP fait appel à deux modules `WapitiTrain` et `WapitiLabel`.
Ces deux modules doivent spécifier:

- le label de chaque token,
- un ensemble de propriétés pour chaque token (POS-tag, lemme, etc.),
- le contexte précise à examiner pour chaque token (par exemple: « le lemme des 2 tokens avant et après »).

Les deux premiers sont spécifiés grâce à un paramètre, le label étant considéré comme la dernière propriété.
La spécification du contexte se fait par l'intermédiaire d'un fichier en utilisant une syntaxe spécifique à Wapiti.


## Répertoire de travail

```
exercices/7-apprentissage
```

## Exercices

### Observer [`apprentissage.plan`](exercices/7-apprentissage/apprentissage.plan) et [`patterns.crf`](7-apprentissage/patterns.crf)

Les quatre premières étapes préparent les données:

- *read* lit les documents contenant les entités de référence,
- *merge-entities* crée un layer *reference* contenant toutes les entités géniques de référence,
- *segmentation* tokenise et segmente en phrases,
- *pos* lemmatise les tokens et leur assigne un POS-tag.

L'étape *train* consiste à entraîner un classifieur avec Wapiti.
Observons les paramètres:

- *modelFile* indique le chemin du fichier où Wapiti enregistre le modèle appris,
- *features* énumère les attributs de chaque token (lemme, POS-tag) et son label (traduction en BIO des entités de la layer *reference*),
- *patternFile* indique le chemin du fichier qui spécifie le contexte à explorer pour chaque token,
- *trainAlgorithm* spécifie l'algorithme d'optimisation de la recherche dans l'espace d'exploration,
- *commandLineOptions* spécifie des options supplémentaires à Wapiti (n'utiliser qu'un seul thred).

L'étape *label* exploite le modèle appris pour prédire un label BIO sur chaque token.
Les paramètres *modelFile* et *features* sont analogues à ceux de l'entraînement.
*labelFeature* spécifie le nom de la feature contenant le label prédit.

L'étape *decode-bio* décode les labels BIO prédits pour créer des annotations dans la layer *prediction*.

Enfin, l'étape *evaluate* compare les layer *reference* et *prediction*.

La syntaxe du fichier `patterns.crf` est documentée [ici](https://taku910.github.io/crfpp/#templ).
Chaque ligne indique un attribut du contexte à examiner pour chaque token.
Par exemple, la ligne:

```
U0m1:%x[-1,0]
```

`U0m1` est un nom arbitraire (mais il doit commencer par `U`).
`%x` introduit des coordonnées.
Les coordonnées `[-1,0]` signifient: un token en arrière (offset `-1`), le lemme (colonne `0`).

Dans l'état actuel, le fichier indique qu'il faut apprendre en examinant
- le contexte dans une fenêtre de deux tokens avant et après (`-2`, `-1`, `1` et `2`),
- le lemme (`0`) et le POS-tag (`1`).

Note: il n'y a pas d'examen du token lui-même (offset `0`), l'apprentissage se fait exclusivement à partir des tokens environnants.


### Exécuter [`apprentissage.plan`](exercices/7-apprentissage/apprentissage.plan) et observer le résultat

À observer:

- le temps d'apprentissage,
- sur le browser: les layer *reference* et *prediction*,
- sur le browser: la feature *bio-gene* sur chaque annotation de la layer *words*,
- l'évaluation consignée dans `evaluation.txt`.

### Observer et exécuter [`entrainement.plan`](exercices/7-apprentissage/entrainement.plan) et [`evaluation.plan`](exercices/7-apprentissage/evaluation.plan)

Le plan précédent a été séparé en deux de façon à évaluer les prédictions faites sur des documents distincts de ceux utilisés pour l'apprentissage.
En effet, dans un cadre d'apprentisage supervisé, on ne doit pas utiliser les mêmes données pour apprendre et évaluer.
Sinon, on favorise ce qu'on appelle l'apprentissage *par coeur*.

À observer:

- le chargement de deux ensembles de documents distincts,
- les étapes identiques avant apprentissage/prédiction,
- les attributs et contexte identiques pour l'apprentissage et la prédiction,
- la différence de performance par rapport au plan précédent (effet de l'apprentissage par coeur).

### Perfectionner le modèle

Pour augmenter les performances de REN par apprentissage, nous pouvons jouer sur:

- augmenter le nombre de documents avec des entités de référence (coûteux),
- l'algorithme d'optimisation (paramètre *trainAlgorithm*),
- les attributs de chaque token (paramètre *features*),
- la spécification du contexte (fichier `patterns.crf`).

Si nous ajoutons des attributs aux tokens, nous devons nécessairement ajouter des lignes au fichier `patterns.crf` afin que Wapiti en tienne compte.
Aussi les attributs et le fichier doivent être les mêmes de l'apprentissage à la prédiction.

Quelques idées pour améliorer les performances:

- augmenter ou diminuer la fenêtre du contexte,
- attributs liés à la forme particulière des noms de gènes,
- présence du token dans le dictionnaire de noms de gènes.
