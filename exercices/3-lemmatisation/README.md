# Exercice 3 : Lemmatisation

Dans l'exercice précédent, nous avons détecté des entités qui prennent la forme d'un syntagme nominal.
Pour ce genre d'entités, la technique de projection de lexique est limitée par le fait que les occurrences dans le texte sont *infléchies*.

Le lemme est la forme non-infléchie d'un mot.
Dans ce nouvel exercice, nous allons apprendre comment *lemmatiser* les mots.

Au préalable, cela signifie que l'on doit segmenter le texte en mots.
De plus les outils pour la lemmatisation fonctionnent mieux si on leur fournit une segmentation en phrases.

## Répertoire de travail

```
exercices/3-lemmatisation
```

## Exercices

### Observer [`lemmatisation.plan`](exercices/3-lemmatisation/lemmatisation.plan)

Ce plan ressemble au résultat de l'exercice précédent.

Nous allons le perfectionner.

### Ajouter des étapes de segmentation

Ajouter deux modules pour la segmentation en mots et en phrases respectivement.

La bibliothèque de AlvisNLP comprend quelques [classes de modules pour la segmentation](https://bibliome.github.io/alvisnlp/reference/Module-reference#segmentation).

Exécuter le plan et observer avec le navigateur les *layer* *words* et *sentences*.

### Ajouter une étape de lemmatisation

La lemmatisation va très souvent de pair avec le *POS-tagging* (Part Of Speech) ou *analyse morphosyntaxique*, c'est à dire l'attribution d'un catégorie syntaxique à chaque mot (verbe, nom, adjectif, etc.)
Les outils font souvent les deux à la fois.
La lemmatisation et le POS-tagging font partie de la catégorie des [traitements linguistiques](https://bibliome.github.io/alvisnlp/reference/Module-reference#linguistic-processing).

Exécuter le plan et observer avec le navigateur les *features* *pos* et *lemma* qui ont été ajoutées aux annotations dans la *layer* *words*.

### Ajouter un module de projection sur les lemmes

Nous allons refaire une étape de projection du même lexique, pour cela nous copions la spécification du module `project-obo` afin de la modifier.
La présence des deux projections nous permettra de comparer la projection sur le texte et la projection sur le lemme.

Au lieu de rechercher les entrées dans le texte du document, nous allons le chercher sur les lemmes des mots.
Cette fonctionnalité est spécifiée grâce au paramètre `subject`:

```xml
<subject layer="words" feature="lemma" />
```

Observer grâce au log la différence du nombre d'annotations entre la projection "brute" et la projection sur les lemmes.

Observer des pluriels annotés:
- "cells"
- "cell poles"
- "transcription factors"
- "septa"
- "signalling pathways"
- "stationary phases"
- "ABC transporters"
- "phosphatases"
- "autolysin activities"

**Attention:** n'oubliez pas de donner à ce module un autre identifiant (sinon observer la réaction de AlvisNLP).

**Attention:** n'oubliez pas de consigner les annotations créées à partir de la projection sur les lemmes dans une *layer* distincte, afin de pouvoir comparer à l'aide du navigateur.

**Attention:** n'oubliez pas de retirer les annotations chevauchantes.

