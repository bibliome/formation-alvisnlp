# Exercice 6 : Notation BIO

Cet exercice a pour objectif de se familiariser avec la notation BIO.


## Répertoire de travail

```
exercices/6-bio
```

## Exercices

### Observer [`decode-bio.plan`](exercices/6-bio/decode-bio.plan)

Le module `GeniaTagger` intègre aussi la REN de noms de gènes et protéines. Cette fonctionnalité est activée par le paramètre `entityFeature`:

```
  <lemma class="GeniaTagger">
    <entityFeature>genia-entity</entityFeature>
  </lemma>
```

Les entités sont inscrites dans la *feature* `genia-entity` au format BIO.
Par exemple la phrase suivante:

> Most cot genes, and the gerE gene, are transcribed by sigmaK RNA polymerase.

présentera après segmentation et le traitement par `GeniaTagger` une layer *words* comme suit:

| form        | lemma      | pos | genia-entity |
|-------------|------------|-----|--------------|
| Most        | Most       | JJS | O            |
| cot         | cot        | NN  | O            |
| genes       | gene       | NNS | O            |
| ,           | ,          | ,   | O            |
| and         | and        | CC  | O            |
| the         | the        | DT  | O            |
| gerE        | gerE       | NN  | B-DNA        |
| gene        | gene       | NN  | I-DNA        |
| ,           | ,          | ,   | O            |
| are         | be         | VBP | O            |
| transcribed | transcribe | VBN | O            |
| by          | by         | IN  | O            |
| sigmaK      | sigmaK     | NN  | B-protein    |
| RNA         | RNA        | NN  | I-protein    |
| polymerase  | polymerase | NN  | I-protein    |
| .           | .          | .   | O            |

### Décoder le BIO

Ajouter une étape juste après l'exécution de `GeniaTagger` qui décode la notation BIO dans la feature *genia-entity*.

Cette étape devra créer des annotations représentant indifféremment gènes et protéines dans le layer *predictions*.

Le layer *predictions* est utilisée lors de l'étape suivante pour mesurer les performances de `GeniaTagger` en REN.

L'étape de décodage pourra faire appel au module `PatternMatcher`.

### Exécuter le plan

Comparer les résultats de `GeniaTagger` avec ceux obtenus avec le dictionnaire ou les patrons.
