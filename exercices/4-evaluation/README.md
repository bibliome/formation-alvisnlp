# Exercice 4 : Évaluation

Dans les précédents exercices, nous avons jugé de la qualité de la détection en visualisant les résultats sur quelques documents.
Ce mode d'évaluation reste très subjectif, incomplet et long.

Lors de cet exercice nous allons estimer la qualité de la Reconnaissance d'Entités Nommées en comparant les prédictions avec une annotation de référence en utilisant des mesures standard (Rappel, Précision et F-score).
Nous allons utiliser les données [Bacteria Biotopes du challenge BioNLP Open Shared Task 2019](https://sites.google.com/view/bb-2019/home) pour estimer la qualité de la REN pour des entités représentant des habitats et phénotypes microbiens.

## Répertoire de travail

```
exercices/4-evaluation
```

## Exercices

### Observer [`evaluation.plan`](evaluation.plan)

Ce workflow comporte plusieurs nouveautés :
* l'étape de lecture utilise le module `BioNLPSTReader` qui charge des documents et des annotations au format BioNP-ST,
* l'étape de projection utilise le lexique de l'ontologie `OntoBiotope` contenant les habitats et phénotypes microbiens,
* l'étape final fait appel au module `LayerComparator` qui produit des scores en comparant les annotations dans des *layers* de référence et dans des *layers* de prédiction.

### Observer les annotations de référence

Exécuter le plan [`evaluation.plan`](evaluation.plan) et observer avec le navigateur les annotations de référence placées dans les *layers* `Habitat` et `Phenotype`.
Ces annotations ont été créées à la main par des experts.
Notez aussi la présence de la *layer* `Microorganism` contenant les annotations de noms de taxons microbiens.

### Observer le contenu du fichier `evaluation.txt`

Ce fichier est produit par le module `LayerComparator`.
Il indique pour chaque document:
* les faux positifs,
* faux négatifs,
* le nombre de vrais positifs,
* les annotations dont les bornes sont incorrectes,
* les scores de rappel, précision et F-score pour un appariement strict et relaché.

À la fin de ce fichier sont donnés les scores pour l'ensemble du corpus.

```
$ tail evaluation.txt 

Global scores:
    Strict Recall      20.467836%
    Strict Precision   43.508287%
    Strict F1          27.839152

    Relaxed Recall      37.751787%
    Relaxed Precision   80.248619%
    Relaxed F1          51.347768

```
