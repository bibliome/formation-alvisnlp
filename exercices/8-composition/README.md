# Exercice 8 : Décomposition de plans

Où nous apprendrons comment un plan peut être décomposé en sous plans.

La décomposition des plans permet:
1. de structurer et organiser les plans lorsqu'ils deviennent complexes,
2. de partager des traitements communs à plusieurs applications.


## Répertoire de travail

```
exercices/8-composition
```

## Exercices

### Observer et exécuter [`entrainement.plan`](exercices/8-composition/entrainement.plan) et [`evaluation.plan`](exercices/8-composition/evaluation.plan)

Ces deux plans ressemblent à ceux de [l'exercice précédent](../7-apprentissage).
Dans les deux fichiers, les étapes après la lecture des documents et avant l'entraînement ou la prédiction sont déplacés dans le fichier `pretraitement.plan`.
Les étapes sont remplacées par un appel à ce fichier:

```
  <pretraitement href="pretraitement.plan"/>
```

Cette ligne indique que l'étape n'est pas une instance d'un module dans la bibliothèque, mais une référence à un autre plan contenu dans le fichier `pretraitement.plan`.
Cette organisation est plus compacte et évite la duplication de code.

### Modulariser la prédiction

1. Déplacer les étapes *label* et *decode-bio* dans un nouveau plan de façon à disposer d'un module de détection de noms de gènes.
2. Modifier `evaluation.plan` de façon à y faire appel.
3. Créer un plan qui lit les fichiers PDF ou PubMed et annote des noms de gènes.

### Déplacer l'étape *read* dans [`pretraitement.plan`](../8-composition/pretraitement.plan)

`pretraitement.plan` doit être en mesure de passer les paramères *textDir* et *a1Dir* à l'étape *read*:

```xml
<alvisnlp-plan id="pretraitement">
  <param name="txt">
    <alias module="read" param="textDir"/>
  </param>

  <param name="a1">
    <alias module="read" param="a1Dir"/>
  </param>

  <read class="BioNLPSTReader"/>

  <segmentation>
```

Le plan commence par la déclaration de deux paramètres nommés *txt* et *a1* dont la valeur sera transmise respectivement aux paramètres *textDir* et *a1Dir* de l'étape *read*.

Ce plan s'utilise ainsi, de la même manière que les modules de la bibliothèque:

```xml
  <pretraitement href="pretraitement.plan">
    <txt>...</txt>
    <a1>...</a1>
  </pretraitement>
```

Modifier `entrainement.plan` et `evaluation.plan`.