#!/usr/bin/python3

import csv
import sys

for row in csv.DictReader(sys.stdin, delimiter=',', quotechar='"'):
    for name in row['names'].split(';'):
        if name != '':
            print ('%s\t%s' % (name, row['locus']))

