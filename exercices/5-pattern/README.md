# Exercice 5 : Patrons d'extraction

Lors de cet exercice, nous allons tenter d'améliorer la performance de la REN en écrivant des patrons d'extraction.


## Répertoire de travail

```
exercices/5-pattern
```

## Exercices

### Observer [`pattern.plan`](exercices/5-pattern/pattern.plan)

Les étapes `regexp` et `sigma` fornt appel au module `PatternMatcher`.

Ce module permet de specifier des patrons grâce à un langage régulier.
Les patrons sont analogues aux expression régulières, mais s'appliquent à une séquence de mots plutôt qu'à une séquence de caractères.

L'étape `regexp` tente de reconnaitre les mots qui ont un aspect de nom de gène.
Alors que l'étape `sigma` cible les mots ou suite de mots qui désignent un facteur *sigma*.

Les clauses entre crochets (`[` / `]`) sont des expressions booléennes qui spécifient des mots à matcher.

Les clauses peuvent être suivies par des quantificateurs, avec la même syntaxe que les expressions régulières:

| Quantificateur | Nombre de mots                                             |
|----------------|------------------------------------------------------------|
| *aucun*        | exactement 1                                               |
| `?`            | `0` ou `1`                                                 |
| `*`            | `0` ou plus                                                |
| `+`            | `1` ou plus                                                |
| `{N}`          | exactement `N`                                             |
| `{N,M}`        | au moins `N` et au plus `M`<br>entre `N` et `M` (inclusif) |
| `{N,}`         | au moins `N`<br>`N` ou plus (inclusif)                     |
| `{,M}`         | au plus `M`<br>entre `0` et `M` (inclusif)                 |

À l'intérieur des clauses l'opérateur `@` permet de spécifier une feature du mot.

Différents opérateurs permettent de comparer les features:

| Opérateur | Résultat                                                                                                                          |
|-----------|-----------------------------------------------------------------------------------------------------------------------------------|
| `A == B`  | `A` est identique à `B`                                                                                                           |
| `A != B`  | `A` est différent de `B`                                                                                                          |
| `A ?= B`  | `A` inclut `B`                                                                                                                    |
| `A ^= B`  | `A` commence par `B`                                                                                                              |
| `A =^ B`  | `A` termine par `B`                                                                                                               |
| `A =~ B`  | `A` matche l'expression régulière `B`<br>([syntaxe Java](https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html)) |

Le langage d'expression offre une multitude d'opérateurs et de fonctions.
Il permet aussi de naviguer dans la structure de données, voir l'[introduction](https://bibliome.github.io/alvisnlp/Element-expression-examples) et la [référence](https://bibliome.github.io/alvisnlp/Element-expression-reference).

### Exécuter [`pattern.plan`](exercices/5-pattern/pattern.plan)

Exécuter le plan et comparer les performances des patrons en REN à celles du dictionnaire.

Commenter à tour de rôle les étapes `regexp` et `sigma` pour déterminer leur contribution respective.

### Concevoir des patrons

Ajouter des étapes pour détecter:

1. les enzymes reconnaissables car il s'agit d'un mot terminé par "*-ase*" éventuellement précédé d'un nom ou d'un adjectif ("*phosphatase*", "*serine phosphatase*").
2. les noms de gènes qui précèdent les mots "*protein*", "*gene*", ou "*promoter*" ("*recA gene*", "*gerE promoter*").

Exécuter et mesurer la puissance des patrons individuellement et ensemble.

Concevoir d'autres patrons à partir de votre expertise ou de l'observation des documents.

Mesurer.

Expérimenter.
