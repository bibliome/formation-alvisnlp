# Exercice 1 : Projection d'un dictionnaire sur des fichiers texte

Dans cet exercice, nous travaillons autour d'un plan simple consistant en:
1. lire des documents à partir de fichiers texte,
2. chercher dans les documents des entrées d'un lexique spécifié par un fichier tabulé,
3. exporter les occurrences détectées dans un fichier.

Nous exécuterons la ligne de commande permettant d'exécuter ce plan et nous nous familiariserons avec les logs de AlvisNLP.
En effet AlvisNLP affiche des informations complètes sur le contexte d'exécution, l'analyse statique du plan, la synthèse de chaque module exécuté et la synthèse de l'exécution.

Nous examinerons les différentes parties du plan afin de comprendre comment sont spécifiées et paramétrées les étapes successives de traitement.

Nous utiliserons aussi le navigateur de la structure de données interne de AlvisNLP qui permet de visualiser les résultats du traitement des textes.
Ce navigateur sera utile lors des exercices suivants pour comprendre la fonctionnalité des différents modules étudiés.

## Répertoire de travail

Les fichiers nécessaires à cet exercice sont rassemblés dans un répertoire.
Pour s'y rendre, changer le répertoire de travail:

```
$ cd exercices/1-projection
```

## Exercices

### Lancer AlvisNLP en spécifiant le plan.

```
$ alvisnlp project-on-txt.plan
```


### Observer les logs

#### 1. Contexte d'exécution

```
[2020-01-23 08:42:22.292][alvisnlp] build version: 0.5rc-SNAPSHOT
[2020-01-23 08:42:22.293][alvisnlp] remote URL: https://rbossy@github.com/Bibliome/alvisnlp.git
[2020-01-23 08:42:22.293][alvisnlp] commit id: 33da0948f5bebf5557bd33cb5038ea5719f0464d
[2020-01-23 08:42:22.295][alvisnlp] commit time: Wed Nov 27 14:58:18 CET 2019
[2020-01-23 08:42:22.295][alvisnlp] branch: issue-105_plan-analysis
[2020-01-23 08:42:22.297][alvisnlp] WARNING dirty build
[2020-01-23 08:42:22.297][alvisnlp] build host: jj-1404-port220
[2020-01-23 08:42:22.297][alvisnlp] build time: Thu Dec 19 17:39:59 CET 2019
[2020-01-23 08:42:22.300][alvisnlp] java home: /home/rbossy/dist/jdk1.8.0_121
[2020-01-23 08:42:22.301][alvisnlp] user: rbossy
[2020-01-23 08:42:22.301][alvisnlp] class path: /home/rbossy/code/alvisnlp/.test/alvisnlp/lib/*:
[2020-01-23 08:42:22.301][alvisnlp] java version: 1.8.0_121
```

#### 2. Analyse des ressources

```
[2020-01-23 08:42:22.348][alvisnlp] loading plan from project-on-txt.plan
[2020-01-23 08:42:22.398][alvisnlp] finished loading project-on-txt.plan
[2020-01-23 08:42:22.402][alvisnlp] External resources
[2020-01-23 08:42:22.403][project-on-txt.read] R: sourcePath = BioNLP-ST-2013_Gene_Regulation_Network_train
[2020-01-23 08:42:22.403][project-on-txt.project] R: dictFile = subtiwiki-genes_2020-01-21.txt
[2020-01-23 08:42:22.404][project-on-txt.export] R: corpusFile = /home/rbossy/code/formation-alvisnlp/exercices/1-projection/export.txt
[2020-01-23 08:42:22.404][project-on-txt.export] R: outDir = /home/rbossy/code/formation-alvisnlp/exercices/1-projection/.
[2020-01-23 08:42:22.450][alvisnlp] temporary directory: /tmp/alvisnlp2288071150476293021
```

#### 3. Analyse de l'usage de la structure de données

```
[2020-01-23 08:42:22.453][alvisnlp] usage for section names
[2020-01-23 08:42:22.453][alvisnlp] WARNING section text used only once by project-on-txt.read
[2020-01-23 08:42:22.453][alvisnlp] usage for layer names
[2020-01-23 08:42:22.453][alvisnlp] WARNING layer genes used only once by project-on-txt.project
[2020-01-23 08:42:22.453][alvisnlp] no usage for relation names
[2020-01-23 08:42:22.453][alvisnlp] no usage for argument names
[2020-01-23 08:42:22.453][alvisnlp] usage for feature names
```

#### 4. Exécution

```
[2020-01-23 08:42:22.453][alvisnlp] start
[2020-01-23 08:42:22.454][project-on-txt] processing
[2020-01-23 08:42:22.455][project-on-txt.read] processing
[2020-01-23 08:42:22.465][project-on-txt.read] done in 10 ms
[2020-01-23 08:42:22.466][project-on-txt.project] processing
[2020-01-23 08:42:22.468][project-on-txt.project] reading dictionary from: subtiwiki-genes_2020-01-21.txt
[2020-01-23 08:42:22.469][project-on-txt.project] we expect lines with 2 columns
[2020-01-23 08:42:22.491][project-on-txt.project] searching...
[2020-01-23 08:42:22.518][project-on-txt.project] found 78 matches
[2020-01-23 08:42:22.518][project-on-txt.project] done in 52 ms
[2020-01-23 08:42:22.518][project-on-txt.export] processing
[2020-01-23 08:42:22.530][project-on-txt.export] done in 11 ms
[2020-01-23 08:42:22.530][project-on-txt] done in 75 ms
[2020-01-23 08:42:22.531][alvisnlp] finished
```

#### 5. Rapport

```
[2020-01-23 08:42:22.531][alvisnlp] annotations: 78
[2020-01-23 08:42:22.531][alvisnlp] postings: 78
[2020-01-23 08:42:22.531][timer] Hierarchical timer summary:
[2020-01-23 08:42:22.532][timer]      230   100.00%   alvisnlp
[2020-01-23 08:42:22.533][timer]      142        61.89%   load-plan
[2020-01-23 08:42:22.533][timer]       75        32.62%   project-on-txt
[2020-01-23 08:42:22.534][timer]       52            69.39%   project
[2020-01-23 08:42:22.534][timer]       22                43.26%   create-trie
[2020-01-23 08:42:22.534][timer]       29                56.74%   (misc)
[2020-01-23 08:42:22.534][timer]       11            15.51%   export
[2020-01-23 08:42:22.535][timer]        2                24.54%   write
[2020-01-23 08:42:22.535][timer]        8                75.46%   (misc)
[2020-01-23 08:42:22.535][timer]       10            13.33%   read
[2020-01-23 08:42:22.536][timer]        1             1.77%   (misc)
[2020-01-23 08:42:22.536][timer]       12         5.49%   (misc)
[2020-01-23 08:42:22.537][timer] Time spent by effective module:
[2020-01-23 08:42:22.539][timer]       52    70.63%   project-on-txt.project
[2020-01-23 08:42:22.539][timer]       11    15.79%   project-on-txt.export
[2020-01-23 08:42:22.539][timer]       10    13.57%   project-on-txt.read
[2020-01-23 08:42:22.540][timer] Time spent by task category:
[2020-01-23 08:42:22.540][timer]      165    71.68%   load resource
[2020-01-23 08:42:22.540][timer]       62    27.07%   module processing
[2020-01-23 08:42:22.541][timer]        2     1.24%   export
```

### Observer `project-on-txt.plan`

Le plan est composé des parties suivantes :

#### 1. En-tête

```xml
<alvisnlp-plan id="project-on-txt">
```

`project-on-txt` est le nom que l'on choisit pour ce plan.
Ce nom apparait dans les messages du log.

#### 2. Module de lecture de fichiers

```xml
  <read class="TextFileReader">
    <source>../share/BioNLP-ST-2013_Gene_Regulation_Network_train</source>
  </read>
```

`read` est le nom que l'on donne à cette étape du traitement.
Ce nom sera repris dans les messages du log.

`TextFileReader` est la *classe* du module, indique la fonctionnalité de ce traitement.
`TextFileReader` est l'une des classes disponibles dans la [bibliothèque de modules](https://bibliome.github.io/alvisnlp/reference/Module-reference).

`sourcePath` est le nom d'un paramètre du module `TextFileReader`, en l'occurrence celui-ci indique le chemin où lire des fichiers texte.

La documentation de [`TextFileReader`](https://bibliome.github.io/alvisnlp/reference/module/TextFileReader) indique la fonctionnalité du module ainsi que l'ensemble des paramètres reconnus et obligatoires.

#### 3. Module de projection de dictionnaires

```xml
  <project class="TabularProjector">
    <dictFile>../share/subtiwiki-genes_2020-01-21.txt</dictFile>
    <targetLayer>genes</targetLayer>
    <valueFeatures>name,locus-tag</valueFeatures>
    <constantAnnotationFeatures>type=Gene</constantAnnotationFeatures>
  </project>
```

`dictFile` est le paramètre du module `TabularProjector` qui sert à spécifier le chemin où trouver le dictionnaire.
Observer le fichier `subtiwiki-genes_2020-01-21.txt`. Il est composé de deux colonnes: la première contient les noms de gènes et la seconde le locus tag.
Ce module recherche les occurrences des valeurs de la première colonne dans les textes et range le résulat (les matches) dans la structure de données.

`targetLayerName` est le paramètre qui indique dans quel *layer* AlvisNLP doit placer les match, à chaque match correspondra une *annotation*.

`valueFeatures` est le paramètre qui indique dans quels *features* AlvisNLP doit placer la valeur de chaque colonne.
Pour chaque *annotation* correspondant à un match, AlvisNLP renseigne une *feature* par colonne.
À cette étape, nous avons choisi de nommer la première colonne `name` et la seconde `locus-tag`.  <!-- bien expliquez cette partie, pas évidente pour un novice, un schéma visuel peut aider -->

`constantAnnotationFeatures` est un paramètre optionel qui sert à ajouter des *features* à chaque annotation créée.
Ici on s'en sert pour renseigner le type des annotations.

#### 4. Module d'export des annotations

```xml
  <export class="TabularExport">
    <outDir>.</outDir>
    <corpusFile>export.txt</corpusFile>
    <lines>documents.sections.layer:genes</lines>
    <columns>
      section.document.@id,
      section.@name,
      @form,
      start ^ "-" ^ end,
      @name,
      @locus-tag
    </columns>
  </export>
```

Ce module écrit les gènes dans le fichier `export.txt`.
Obsever ce fichier.

### Lancer le navigateur d'annotation

```
$ alvisnlp -browser project-on-txt.plan
```

L'option `-browser` suspend l'exécution juste après le dernier module et expose les annotations sous forme d'un serveur web local.
Les logs indiquent:

```
[2020-01-23 09:52:42.514][project-on-txt.annotation-browser] processing
[2020-01-23 09:52:42.548][project-on-txt.annotation-browser] server started on http://localhost:8878
[2020-01-23 09:52:42.548][project-on-txt.annotation-browser] hit enter to stop server and proceed to next module
```

Ouvrir l'URL [`http://localhost:8878`](http://localhost:8878) sur un navigateur.
Observer et critiquer l'annotation.

Pour terminer l'exécution d'AlvisNLP et reprendre la main, appuyer sur *Entrée*.

### Modifier le plan

Compléter les paramètres de `project` afin d'annoter les gènes, ainsi que les protéines (noms capitalisés). Conférez-vous à la documentation du module [TabularProjector](https://bibliome.github.io/alvisnlp/reference/module/TabularProjector).


