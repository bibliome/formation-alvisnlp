## Titre

Introduction au text-mining avec AlvisNLP

## Objectifs pédagogiques

Cette formation est dédiée à l'analyse de données textuelles (text-mining). L'objectif est l'acquisition des principales techniques pour la Reconnaissance d'Entités Nommées (REN) à partir de textes. Les entités nommées étudiées dans cette formation sont des objets ou concepts d'intérêts mentionnés dans les articles scientifiques ou les champs en texte libre (taxons, gènes, protéines, marques, etc.).

Les participants vont acquérir les compétences pratiques nécessaires pour effectuer de façon autonome une première approche pour une application de text-mining. Le format est celui de Travaux Pratiques utilisant AlvisNLP, un outil de composition de workflows en text-mining développé par l'équipe Bibliome de l'unité MaIAGE. La formation s'adresse à des chercheurs et ingénieurs en (bio)-informatique ou en maths-info-stats appliquées.

## Durée et mode

* 2 jours en présentiel

## Programme

* Présentation du text-mining et de la Reconnaissance des Entités Nommées (REN)
* Travaux Pratiques sur des techniques de REN en utilisant [AlvisNLP](https://github.com/Bibliome/alvisnlp)
    * Projection de lexiques
    * Application de patrons
    * Apprentissage automatique

## Prérequis

* Une connaissance de la ligne de commande Linux est requise.
* Familiarité avec la manipulation de fichiers de données (conversion, filtrage, visualisation).
