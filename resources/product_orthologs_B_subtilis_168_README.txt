README_jeux_donnees_product_orthologues_WP_TEXT


Grouper les annotations "Product" qui devraient être identiques. Le problème est l’hétérogénéité de la sémantique utilisée pour décrire la même annotation fonctionnelle.
Sur les 3 examples suivants :


**********************************
*** dnaA BSU00010 (Bacillus subtilis subsp. subtilis str. 168) :
Product : chromosomal replication initiator protein DnaA
Environ 70 variations syntaxique repérée parmis un jeu de 2584 orthologues putatifs

Résumé des problèmes :
	- variation syntaxe : "chromosomal" OU "chromosome", "initiation" OU "initiator"
	- mot facultatifs générique présent/absent : : related to, factor, putative, involved in, protein, probable, nom du gene avec parfois rajout numéro sous-unité (dnaA ou DnaA1)
	- ajout de ponctuation : virgule, point virgule, tiret, ect.
	- ajout de commentaire à la fin entre parenthèse ou après ponctuation: DNA-binding transcriptional dual regulator
	- majuscule/minuscule

	- ordre des mots différents
	- mot facultatifs spécifique présent/absent : DNA-directed,  chromosomal, bacterial


**********************************
*** guaB BSU00090 (Bacillus subtilis str. 168)
Product : inosine-monophosphate dehydrogenase
Environ 100 variations syntaxique repérée parmis un jeu de 2322 orthologues putatifs

Résumé des problèmes :
	- variation syntaxe : "monophosphatedehydrogenase" OU "monophosphate dehydrogenase"
	- mot facultatifs générique présent/absent : related to, factor, putative, involved in, protein, probable, nom du gene avec parfois rajout numéro sous-unité (dnaA ou DnaA1)
	- ajout de ponctuation : virgule, point virgule, tiret, ect.
	- ajout de commentaire : Predicted signal-transduction protein containing cAMP-binding and CBS domains, IMP dehydrogenase
	- majuscule/minuscule

	- synonyme : "Inosinic acid" OU "inosine monophosphate"
	- Abreviation : "inosine-5'-monophosphate dehydrogenase" OU "IMP dehydrogenase" OU "IMPDH" OU "IMPD"
	- mot facultatifs spécifique présent/absent : 5'-, 
	- Attention : parfois 5-Inositol, parfois Inosine = 2 molecules différentes


**********************************
*** dacA BSU00100 (Bacillus subtilis str. 168)
Product : D-alanyl-D-alanine carboxypeptidase (penicillin-binding protein 5)
Environ 50 variations syntaxique repérée parmis un jeu de 487 orthologues putatifs

	- variation syntaxe : "D-Ala-D-Ala" OU "D-alanyl-D-alanine" OU "DD" OU "D-alanine"
	- mot facultatifs générique présent/absent : related to, factor, putative, involved in, protein, probable, nom du gene avec parfois rajout numéro sous-unité (dnaA ou DnaA1)
	- ajout de ponctuation : virgule, point virgule, tiret, ect.
	- ajout de commentaire : 
	- majuscule/minuscule

	- Abreviation : "penicillin-binding protein" OU "PBP3" 
	- numéro de sous unité différent : 3, 4 ou 5
	- Parfois juste "D-alanyl-D-alanine carboxypeptidase" ou juste "penicillin-binding protein"











