# How to Install and Use AlvisNLP

## Sur votre machine ([singularity >= 3.6 must be installed](https://sylabs.io/guides/3.7/user-guide/quick_start.html#quick-installation-steps))

1. Télécharger le matériel pédagogique

```
$ git clone https://forgemia.inra.fr/bibliome/formation-alvisnlp.git && cd formation-alvisnlp
```

2. Télécharger l'image disponible sur sylabs https://cloud.sylabs.io

```
$ singularity pull library://migale/default/alvisnlp:0.8.1
```

3. Afficher l'aide d'AlvisNLP (vous pouvez créer un alias `alvisnlp` pour simplifier)

```
$ alias alvisnlp="/chemin/vers/alvisnlp_0.8.1.sif`

$ alvisnlp -help
Container was created 
Arguments received: -help
alvisnlp
    process text corpora

Usage:
    alvisnlp [OPTIONS] PLANFILE

Documentation options:
    -help                                 print this help
    -supportedModules                     print supported modules and exit
    -supportedConverters                  print all types of parameters that can be converted
    -supportedLibraries                   print the name of all supported expression libraries
    -moduleDoc              MODULE        print documentation for a module and exit
...
```

4. Tester le lancement d'un plan

```
$ cd exercices/1-projection/
$ singularity pull library://migale/default/alvisnlp:0.8.1
$ alvisnlp -browser project-on-txt.plan
```

5. Aller à l'URL affiché dans les logs (ex: http://localhost:8878) pour voir les résultats


## Sur votre compte Migale (singularity est déja installé)

1. Connectez-vous sur Migale

```
$ ssh [USER]@migale
Last login: Mon Sep 20 13:42:22 2021 from 138.102.22.129
  __  __ _           _     
 |  \/  (_)__ _ __ _| |___ 
 | |\/| | / _` / _` | / -_)
 |_|  |_|_\__, \__,_|_\___|
          |___/           
-----------------------
Bienvenue sur le serveur frontal de la  plateforme Migale
...
```

2. Télécharger le matériel pédagogique dans votre `work`

```
$ cd ~/work
$ git clone https://forgemia.inra.fr/bibliome/formation-alvisnlp.git && cd formation-alvisnlp
```

3. Télécharger l'image disponible sur sylabs https://cloud.sylabs.io

```
$ singularity pull library://migale/default/alvisnlp:0.8.1
```

4. Afficher l'aide D'AlvisNLP (vous pouvez créer un alias `alvisnlp` pour simplifier)

```
$ alias alvisnlp= "/chemin/vers/alvisnlp_0.8.1.sif`

$ alvisnlp -help
Container was created 
Arguments received: -help
alvisnlp
    process text corpora

Usage:
    alvisnlp [OPTIONS] PLANFILE

Documentation options:
    -help                                 print this help
    -supportedModules                     print supported modules and exit
    -supportedConverters                  print all types of parameters that can be converted
    -supportedLibraries                   print the name of all supported expression libraries
    -moduleDoc              MODULE        print documentation for a module and exit
...
```

5. Tester le lancement d'un plan

```
$ cd exercices/1-projection/
$ singularity pull library://migale/default/alvisnlp:0.8.1
$ alvisnlp -browser project-on-txt.plan
```

6. Aller à l'URL affiché dans les logs (ex: http://localhost:8878) to voir les resultats


---

**NOTE**

Si besoin de monter manuellement les dossiers, la commande `singularity run -B $PWD -B $PWD/.. alvisnlp_0.8.1.sif` peut être utilisée à la place de `./alvisnlp-0.8.1.sif`. L'alias devient par exemple `alias alvisnlp="singularity run -B $PWD -B $PWD/.. alvisnlp_0.8.1.sif"` et on monte les dossiers $PWD` and `$PWD/..`.

---
