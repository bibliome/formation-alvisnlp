#!/bin/bash -       
#title           : install-alvisnlp-min.sh
#description     : This script designed to install alvisnlp ascompagnied with geniatagger and treetagger.
#author		 :Ba
#date            :25022020
#version         :0.1    
#usage		 :install-alvisnlp-min.sh
#bash_version    :4.4.20(1)-release


# general dependencies
apt-get -yqq install software-properties-common
add-apt-repository universe
add-apt-repository ppa:openjdk-r/ppa
apt-get -yqq update
apt-get -yqq install maven 
apt-get -yqq install git
apt-get -yqq install wget
apt-get -yqq install xmlstarlet
apt-get -yqq install zip
apt-get -yqq install python
apt-get -yqq install python-numpy
apt-get -yqq install make
apt-get -yqq install ruby
apt-get -yqq install g++
apt-get -yqq install gcc
apt-get -yqq install libboost-all-dev 
apt-get -yqq install flex
apt-get -yqq install openjdk-8-jdk

# install alvisnlp
cd ~
git clone -b master https://github.com/Bibliome/alvisnlp.git alvisnlp
cd alvisnlp/
mvn clean install
./install.sh .

# install external tools
cd ~/alvisnlp
mkdir extsoftdir
cd extsoftdir/
wget wget https://github.com/saffsd/geniatagger/archive/master.zip
unzip master.zip 
mv geniatagger-master/ geniatagger-3.0.2
rm master.zip 
cd geniatagger-3.0.2/
make
cd ../
wget http://www.cis.uni-muenchen.de/%7Eschmid/tools/TreeTagger/data/tree-tagger-linux-3.2.1.tar.gz
mkdir treetagger
tar xvf tree-tagger-linux-3.2.1.tar.gz -C treetagger
rm tree-tagger-linux-3.2.1.tar.gz 
cd treetagger/
mkdir lib
cd lib/
wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz
gunzip english.par.gz 
cd ../
cd ../
cd ../

# set default params
cd ~/alvisnlp
cp share/default-param-values.xml.template share/default-param-values.xml
cat share/default-param-values.xml | xmlstarlet ed -u "//module[@class='fr.inra.maiage.bibliome.alvisnlp.bibliomefactory.modules.treetagger.TreeTagger']/treeTaggerExecutable" -v $PWD/extsoftdir/treetagger/bin/tree-tagger | tee share/default-param-values.xml
cat share/default-param-values.xml | xmlstarlet ed -u "//module[@class='fr.inra.maiage.bibliome.alvisnlp.bibliomefactory.modules.treetagger.TreeTagger']/parFile" -v $PWD/extsoftdir/treetagger/lib/english.par | tee share/default-param-values.xml
cat share/default-param-values.xml | xmlstarlet ed -u "//module[@class='fr.inra.maiage.bibliome.alvisnlp.bibliomefactory.modules.geniatagger.GeniaTagger']/geniaDir" -v $PWD/extsoftdir/geniatagger-3.0.2/ | tee share/default-param-values.xml

#**** add singularity image to bin ****
### ajout de l'image singularity de 3 go ici
## cp alvisnlp.sif ~/alvisnlp/bin/

# export to path
cd ~/alvisnlp
export PATH=$PATH:$PWD/bin
