#!/bin/bash -   

cd ~/formation-alvisnlp/exercices

cd 1-projection/
alvisnlp project-on-txt.plan
alvisnlp.sif project-on-txt.plan
cd ../

cd 2-composition/
alvisnlp composition.plan
alvisnlp.sif composition.plan
cd ../


cd 3-projecteurs/
alvisnlp projecteurs.plan
alvisnlp.sif projecteurs.plan
cd ../


cd 4-lemmatisation/
cd aide/
alvisnlp lemmatisation.plan
alvisnlp.sif lemmatisation.plan
alvisnlp project-lemma.plan
alvisnlp.sif project-lemma.plan
alvisnlp segmentation.plan
alvisnlp.sif segmentation.plan
cd ../
alvisnlp lemmatisation.plan
alvisnlp.sif lemmatisation.plan
cd ../


cd 5-evaluation/ 
cd aide/
alvisnlp evaluation-lemmes.plan
alvisnlp.sif evaluation-lemmes.plan
cd ../
alvisnlp evaluation.plan
alvisnlp.sif evaluation.plan
cd ../



cd 6-pattern/ 
cd aide/
alvisnlp enzymes.plan
alvisnlp.sif enzymes.plan
alvisnlp promoter.plan
alvisnlp.sif promoter.plan
alvisnlp regexp.plan
alvisnlp.sif regexp.plan
alvisnlp sigma.plan
alvisnlp.sif sigma.plan
cd ../
alvisnlp pattern.plan
alvisnlp.sif pattern.plan
cd ../
