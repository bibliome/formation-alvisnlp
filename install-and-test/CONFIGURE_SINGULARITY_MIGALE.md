## CONFIGURER COTE MIGALE

1. se connecter sur son compte migale et aller sur ton work

```
ssh migale username@migale

cd work/
```

2. télécharger l'image singularity et tester le help

```
singularity pull library://migale/default/alvisnlp:0.8.1

./alvisnlp_0.8.1.sif -help
```

3. ouvrir le fichier `./bash_aliases` et ajouter cette ligne à la fin du fichier 

```
alias alvisnlp="singularity run -B $(pwd -P)  -B $(pwd -P)/..  /work_home/username/alvisnlp_0.8.1.sif"
```


4. tester l'alias `alvisnlp`

```
source ~/.bashrc

alvisnlp -help

mkdir tmp-test && cd tmp-test && type alvisnlp

rm -rf tmp-test
```

la sortie de `type alvisnlp` doit être 

```
alvisnlp est un alias vers « singularity run -B /work_home/username/tmp-test -B /work_home/username/tmp-test/..  /work_home/username/alvisnlp_0.8.1.sif»
```

## CONFIGURER COTE PORTABLE

1. ajouter cette ligne à la fin de ton fichier `~/.bash_aliases` sous son portable

```
alias ssh4alvis = "ssh -L 8878:localhost:8878"
```

2. se connecter avec l'alias `ssh4alvis`

```
ssh4alvis  username@migale
``` 

3. télécharger le projet formation et aller sur un exercice

```
cd /home_work/username
git clone https://forgemia.inra.fr/bibliome/formation-alvisnlp.git && cd formation-alvisnlp/exercices/1-projection/
```

4. lancer avec le browser

```
alvisnlp -browser project-on-txt.plan
```

si `bug` faire `source ~/.bashrc`

5. sous votre portable via le navigateur aller à l'addresse indiquée dans les logs. Si tout fonctionne vous avez accès au browser d'alvis
