## Intro

* qu'est-ce c'est la REN
* difficultés (synonymes, homonymes, variations...)
* -> pourquoi un WF engine
* AlvisNLP c'est quoi
* **modèle de données**
* plan et bibliothèque de modules et sous-plans
* présentation des exercices
* sensibilisation au droit sur: les documents, les ressources, les résultats
* Perspectives: intégration suite Alvis, développement de nouveaux modules, service Migale

https://bibliome.github.io/alvisnlp/

## Exos

* Readers (txt, pubmed XML, html, PDF)
* Browser
* Projectors (tabular, OBO)
* Segmentation, lemmatization
* Regexp
* Pattern
* ToMap?
* EN générales: StanfordNER
* Apprentissage supervisé: WapitiTrain / WapitiLabel
* Embeddings?

## Datasets

### Taxons

* liste NCBI modifiée

### Orthologs

Etant donné un ensemble de protéines othologues (BDBH, synténie), on cherche à comparer le champ "Product" de façon à:
* détecter le faux positifs
* détecter les erreurs d'annotation

### Listeria monocytogenes

* les gènes occurrent peu dans la littérature
* à consolider

### Saccaromyces cerevisae

* tous les gènes de yeastgenome.org

### Bacillus subtilis

* corpus annoté BioNLP-ST 11/13
* liste de gènes de SubtiWiki, mais l'export ne fonctionne pas (http://subtiwiki.uni-goettingen.de/v3/exports)

### Blé

* liste de gènes à déterminer

## Logistique

### Exécution

* PC personnel, les participants installent avant la formation (+: participants repartnt avec le soft, -: ça va pas marcher)
* PC personnel, VM VirtualBox (+: participants repartnt avec le soft, ça peut marcher, -: lourd, travail)
* PC formation (+: maîtrise de l'install, -: participants partent sans le soft, places limitées)
* Migale (+: maîtrise de l'install, c'est fait, -: participants partent sans le soft, charge, éditeurs, PC personnels filaire)

### Matériel pédagogique

* Jupyter (+: c'est cool, -: travail)
* Intro: slides
* Exercices: fichiers Markdown sur le dépôt

### Éditeur

* VIM, Emacs, Nano, GEdit, ~~Eclipse~~

### Licences

* AlvisNLP: ALv2
* Matériel pédagogique: CC0, CC-BY?
