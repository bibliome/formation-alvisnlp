## Introduction au text-mining: Reconnaissance d'Entités Nommées

### Description

L'objectif de cet atelier est l'acquisition des techniques de base pour la Reconnaissance d'Entités Nommées (REN) à partir de textes.
Les entités nommées étudiées dans cet atelier sont des objets ou concepts d'intérêts mentionnés dans les articles scientifiques ou les champs en texte libre (lieux, personnes, marques, taxons, protéines, etc.)
Les participants acquièrent les compétences pratiques nécessaires pour effectuer de façon autonome une première approche pour une application de text-mining.
Le format est celui de Travaux Pratiques utilisant AlvisNLP, un outil de composition de workflows en text-mining développé par l'équipe Bibliome de l'unité MaIAGE.
Cette atelier s'adresse à des chercheurs et ingénieurs en informatique ou en maths-info-stats appliquées ou en bioinformatique.

### Prérequis

* Pratique de la ligne de commande Linux.
* Habitude de manipuler des données scientifiques.
<!-- * Un compte sur la plateforme Migale ([demande de compte gratuite](https://migale.inra.fr/ask-account)). -->

### Connaissances acquises

* introduction à la REN (définition, utilisation, défis)
* workflows de text-mining
* principes de AlvisNLP
* formats usuels en text-mining
* techniques de base pour la REN
  * projection de lexiques
  * segmentation
  * lemmatisation
  * création et application de patrons
  * extraction terminologique
  * apprentissage sur les séquences

### Matériel

AlvisNLP et ses dépendances sont installées dans un conteneur sinularity.

Pendant l'atelier, les participants utiliseront leur propre portable.

Après l'atelier, les participants pourront exercer et utiliser les compétences nouvellement acquises (en se connectant sur Migale ou alors sur leur hôte avec un conteneur.)

Le matériel pédagogique est disponible avec une license libre.

### Contacts

`{Mouhamadou.Ba|Robert.Bossy}@inrae.fr`
