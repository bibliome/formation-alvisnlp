# Exercices

## Hello

### Donné

* Connexion sur `migale`
* Répertoire de travail
* Chemin exécutable
* https://bibliome.github.io/alvisnlp/reference/Module-reference
* éditeurs pour XML
 
### à faire

* `alvisnlp -help`
* `alvisnlp -supportedModules`

## Un plan simple

### Donné

* Plan squelette
  1. `TextFileReader` (prévoir documents)
  2. `TabularProjector` (prévoir dictionnaire: gènes ou taxons)
  3. `TabularExport` (question: expliques les expressions ou pas?)

### à faire

1. Exécuter et observer le log
2. Observer l'export
3. Réexécuter avece l'option `-browser` et observer la structure de données
4. Modifier un paramètre (prévoir un exemple où le match est faux ou incomplet)
  
## Composition des plans

### Donné

* Le plan avec `TabularExport` dans un sous-plan

### à faire

1. Externaliser `TextFileReader`
2. Remplacer `TextFileReader` par `TikaReader` et/ou `XMLReader` (prévoir documents PDF, DOC, XML, HTML, XSLT pour HTML et PubMed)

## Projecteurs

### à faire

1. Remplacer `TabularProjector` par `OBOProjector` et/ou `RDFProjector` (prévoir un fichier OBO et un fichier RDF, refaire la doc pour `RDFProjector`)

## Segmentation et lemmatization

### donné

* le nom des modules à utiliser

### à faire

1. Ajouter `WoSMig`, `SeSMig` et `GeniaTagger` (prévoir un exemple de match incomplet à cause de l'inflexion)
2. Remplacer `WoSMig` et `SeSMig` par `res://segmentation.plan`

## RegExp

* question: est-ce pertinent?

## Patron

### donné

* Hearst pattern (théorie)
* Exemple gènes
* Plan `PatternMatcher`

### à faire

* Modifier le patron (prévoir une ou plusieurs exemples de patrons de match faux ou incomplets)

## Modules prêt à l'emploi

### donné

* le nom des modules

### à faire

1. Ajouter le paramètre `entityFeature` à `GeniaTagger`
2. Ajouter les modules `Chemspot` et `StanfordNER`
3. Décoder le BIO de `GeniaTagger` avec `PatternMatcher` (prévoir du temps)

## ToMap

### donné

* plans avec `TomapTrain`, `TomapProjector`

### à faire

1. Exécuter les deux plans en séquence
2. Obeserver les résultats (prévoir: exemples probants)

