## Réponses de DevLog

### Recensement (Orsay)

Nicolas.Thiery@u-psud.fr

https://hackmd.io/y1L2Bu3qRquE3u3r1qhjQA

### Discord

Bruno.Mermet@univ-lehavre.fr
cyrille.bonamy@univ-grenoble-alpes.fr

### https://codeshare.io/

Bruno.Mermet@univ-lehavre.fr

Éditeur de code en ligne. Orienté interviews, enseignement, maintenance.

### https://framapad.org/

Bruno.Mermet@univ-lehavre.fr

Comme Notepad x Google Docs.

### Zoom

pierre.poulain@u-paris.fr

Avec fonctionalité de contrôle à distance du partage d'écran (payant).

### JupyterHub

pierre.poulain@u-paris.fr

### MeshCentral

laurent.guerby@mines-albi.fr

https://github.com/Ylianst/MeshCentral
https://twitter.com/MeshCentral
https://meshcentral2.blogspot.com/
https://www.valken.org/installer-un-serveur-meshcentral.html
https://it-security.dnit.fr/?p=261&lang=fr

### GitHub Classroom

Geoffroy.Vibrac@cnous.fr

https://classroom.github.com/

Utilise le branche-merge-pull request pour faire l'évaluation des TD.

### Liiibre

sebastien.rey-coyrehourcq@univ-rouen.fr

https://linuxfr.org/news/liiibre-une-solution-complete-pour-vos-projets-collaboratifs

### Visual Studio Live Share

Guillermo.Andrade-barroso@inria.fr

Comme GoogleDoc pour du code.

